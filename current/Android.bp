// *** THIS PACKAGE HAS SPECIAL LICENSING CONDITIONS.  PLEASE
//     CONSULT THE OWNERS AND opensource-licensing@google.com BEFORE
//     DEPENDING ON IT IN YOUR PROJECT. ***
package {
    // See: http://go/android-license-faq
    // A large-scale-change added 'default_applicable_licenses' to import
    // all of the 'license_kinds' from "prebuilts_sdk_license"
    // to get the below license kinds:
    //   SPDX-license-identifier-Apache-2.0
    //   SPDX-license-identifier-BSD
    //   SPDX-license-identifier-CC0-1.0
    //   SPDX-license-identifier-OFL (by exception only)
    //   SPDX-license-identifier-Unicode-DFS
    //   legacy_unencumbered
    default_applicable_licenses: ["prebuilts_sdk_license"],
}

java_sdk_library_import {
    name: "android.test.base",
    public: {
        jars: ["public/android.test.base.jar"],
        sdk_version: "current",
    },
    system: {
        jars: ["system/android.test.base.jar"],
        sdk_version: "system_current",
    },
    test: {
        jars: ["test/android.test.base.jar"],
        sdk_version: "test_current",
    },
}

java_sdk_library_import {
    name: "android.test.runner",
    public: {
        jars: ["public/android.test.runner.jar"],
        sdk_version: "current",
    },
    system: {
        jars: ["system/android.test.runner.jar"],
        sdk_version: "system_current",
    },
    test: {
        jars: ["test/android.test.runner.jar"],
        sdk_version: "test_current",
    },
    libs: [
        "android.test.base",
        "android.test.mock",
    ],
}

java_sdk_library_import {
    name: "android.test.mock",
    public: {
        jars: ["public/android.test.mock.jar"],
        sdk_version: "current",
    },
    system: {
        jars: ["system/android.test.mock.jar"],
        sdk_version: "system_current",
    },
    test: {
        jars: ["test/android.test.mock.jar"],
        sdk_version: "test_current",
    },
}

java_sdk_library_import {
    name: "com.android.future.usb.accessory",
    public: {
        jars: ["public/com.android.future.usb.accessory.jar"],
        sdk_version: "current",
    },
    system: {
        jars: ["system/com.android.future.usb.accessory.jar"],
        sdk_version: "system_current",
    },
    test: {
        jars: ["test/com.android.future.usb.accessory.jar"],
        sdk_version: "test_current",
    },
}

java_sdk_library_import {
    name: "com.android.location.provider",
    public: {
        jars: ["public/com.android.location.provider.jar"],
        sdk_version: "current",
    },
    system: {
        jars: ["system/com.android.location.provider.jar"],
        sdk_version: "system_current",
    },
    test: {
        jars: ["test/com.android.location.provider.jar"],
        sdk_version: "test_current",
    },
}

java_sdk_library_import {
    name: "com.android.mediadrm.signer",
    public: {
        jars: ["public/com.android.mediadrm.signer.jar"],
        sdk_version: "current",
    },
    system: {
        jars: ["system/com.android.mediadrm.signer.jar"],
        sdk_version: "system_current",
    },
    test: {
        jars: ["test/com.android.mediadrm.signer.jar"],
        sdk_version: "test_current",
    },
}

java_sdk_library_import {
    name: "com.android.media.remotedisplay",
    public: {
        jars: ["public/com.android.media.remotedisplay.jar"],
        sdk_version: "current",
    },
    system: {
        jars: ["system/com.android.media.remotedisplay.jar"],
        sdk_version: "system_current",
    },
    test: {
        jars: ["test/com.android.media.remotedisplay.jar"],
        sdk_version: "test_current",
    },
}

java_sdk_library_import {
    name: "com.android.media.tv.remoteprovider",
    public: {
        jars: ["public/com.android.media.tv.remoteprovider.jar"],
        sdk_version: "current",
    },
    system: {
        jars: ["system/com.android.media.tv.remoteprovider.jar"],
        sdk_version: "system_current",
    },
    test: {
        jars: ["test/com.android.media.tv.remoteprovider.jar"],
        sdk_version: "test_current",
    },
}

java_sdk_library_import {
    name: "com.android.nfc_extras",
    public: {
        jars: ["public/com.android.nfc_extras.jar"],
        sdk_version: "current",
    },
    system: {
        jars: ["system/com.android.nfc_extras.jar"],
        sdk_version: "system_current",
    },
    test: {
        jars: ["test/com.android.nfc_extras.jar"],
        sdk_version: "test_current",
    },
}

java_sdk_library_import {
    name: "javax.obex",
    public: {
        jars: ["public/javax.obex.jar"],
        sdk_version: "current",
    },
    system: {
        jars: ["system/javax.obex.jar"],
        sdk_version: "system_current",
    },
    test: {
        jars: ["test/javax.obex.jar"],
        sdk_version: "test_current",
    },
}

java_sdk_library_import {
    name: "org.apache.http.legacy",
    public: {
        jars: ["public/org.apache.http.legacy.jar"],
        sdk_version: "current",
    },
    system: {
        jars: ["system/org.apache.http.legacy.jar"],
        sdk_version: "system_current",
    },
    test: {
        jars: ["test/org.apache.http.legacy.jar"],
        sdk_version: "test_current",
    },
}

java_import {
    name: "android-support-multidex-instrumentation",
    jars: ["multidex/instrumentation/android-support-multidex-instrumentation.jar"],
    sdk_version: "current",
}

java_import {
    name: "android-support-multidex",
    jars: ["multidex/library/android-support-multidex.jar"],
    sdk_version: "current",
}

// To provide test host tools the current android jar file to execute the host test
filegroup {
    name: "current_android_jar",
    srcs: [
        "public/android.jar",
    ],
    visibility: [
         "//system/apex/apexer", // for apexer_host_test_tools
         "//system/apex/tools",  // for apex_compression_test_host_tools
    ],
}
